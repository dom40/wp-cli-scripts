#!/bin/bash

new=$1 #new site url
current=$(wp option get siteurl) #current site url

wp search-replace $current $new 

posts=$(wp post list --field=ID --post_type=page)

SAVEIFS=$IFS   # Save current IFS
IFS=$'\n'      # Change IFS to new line
posts=($posts) # split to array $posts
IFS=$SAVEIFS   # Restore IFS

for (( i=0; i<${#posts[@]}; i++ ))
do
    echo "$i: ${posts[$i]}"
    postname=$(wp post get ${posts[$i]} --field=post_name)
    wp post update ${posts[$i]} --post_name=$postname
done

wp yoast index


